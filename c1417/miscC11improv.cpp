#include <iostream>
//C++ 14

//Fibonacci Numbers. Add last two numbers to get the next. First two are 0 and 1

//Executed at compiletime c++14
constexpr auto fibonacci(int n) {
  if (n == 0) {
    return 0;
  }
  else if (n == 1) {
    return 1;
  }
  else {
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
}

//Varaible templates & specialized.

template <typename T>
constexpr T maxValue = T(1000);

template<>
constexpr double maxValue<double> = 2000;

template<>
constexpr char maxValue<char> = 'z';

// Make this function a Deprecated function
[[deprecated("drumBrakes() are old tech use diskBrakes() instead")]]
void drumBrakes() {
  std::cout << "Drum Brakes are the best Brakes" << std::endl;
}

void diskBrakes() {
  std::cout << "You know disks are better" << std::endl;
}

int main(int argc, char* argv[])
{
  //Compile time computation. Error should be at compile time NOT at Run Time
  //Change 55 to any other number and see red squigly line under static_assert.
  static_assert(fibonacci(10) == 55, "Expected 55");

  //variable templates and specialization
  std::cout << "int specialized is  " << maxValue<int> << std::endl;
  std::cout << "double specialized is  " << maxValue<double> << std::endl;
  std::cout << "char specialized is  " <<maxValue<char> << std::endl;

  //use deprecated feature. You should see a compiler warning.
  drumBrakes();
}