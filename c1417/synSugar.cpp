#include <iostream>

//C++14

//Automatic return type deduction
auto areaOfCircle(double radius) {
  return (3.14) * (radius*radius);
  //more useful on templates or std::map<std::.....>
}


int main(int argc, char* argv[])
{
  //Digit Seperator 
  long x = 10'000'000;
  double y = 10'455.90;
  std::cout << x << std::endl;
  //
  //Binary Literals c++14
  auto d = 0b0101110;
  auto e = 0B0101110'01110110;
  return 0;
  //
  //automatic return type deduction
  double area = areaOfCircle(2.0);

}