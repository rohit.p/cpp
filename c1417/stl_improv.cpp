#include <algorithm>
#include <iostream>
#include <vector>
//C++ 14

class abc {
public:
	abc() {
		std::cout << "In constructor" << std::endl;
	}

	~abc() {
		std::cout << "In destructor" << std::endl;
	}
	void iAmMethod() {
		std::cout << "I am in method" << std::endl;
	}
private:
	int a{};
	double b{ 0.111 };
};
//Varaible templates & specialized.

int main(int argc, char* argv[])
{
	//create and object with new
	abc *x = new abc;
	x->iAmMethod();
	//create an object with make_unique
	std::unique_ptr<abc> x1 = std::make_unique<abc>();

	//Lambdas
	std::vector<std::string> names{ "ab","fdjas","sfa","sfaww","fasfdaq" };
	std::sort(names.begin(),names.end(),
		[](const std::string &a, const std::string &b) {
		return a.length() < b.length(); });
}